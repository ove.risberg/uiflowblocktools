# UIFlow Block tools

UIFlow Block files can be created with http://block-maker.m5stack.com/ but the block code window is small and this is not optimal if you want to create larger blocks.
The java script code is visible on the right side of the screen and you can type in that window but your changes are lost as soon as you do anyting on the left side.
If you want to compare two m5b files it is hard because they only contain one very long line and most diff tools are not good at comparing just one line.

I have made two simple python scripts to convert m5b files to the java script code you see in the right side of the screen and back so it is easier to make large blocks and to compare changes.
These scripts are only tested on Linux with python3 installed.

UIFlow Block Maker is still in beta so the limitations above may change in the future and my scripts may stop working because the m5b format can change so use these tools on your on risk.

## Convert m5b file to java script

    user@ubuntu:~$ cat example.m5b 
    {"category":"example","color":"#333333","blocks":["__example_test"],"jscode":"// Block __example_test\nvar __example_test_json = {\n    \"output\": null,\n    \"colour\": \"#333333\"\n};\n\nwindow['Blockly'].Blocks['__example_test'] = {\n    init: function() {\n        this.jsonInit(__example_test_json);\n    }\n};\n\nwindow['Blockly'].Python['__example_test'] = function(block) {\n        return [`HelloWorld`, Blockly.Python.ORDER_CONDITIONAL]\n};\n\n","code":{"test":["window['Blockly'].Python['__example_test'] = function(block) {\n        return [`HelloWorld`, Blockly.Python.ORDER_CONDITIONAL]\n};\n\n","HelloWorld"]}}
    user@ubuntu:~$ m5b2jscode example.m5b > example.jscode
    user@ubuntu:~$ cat example.jscode
    // Block __example_test
    var __example_test_json = {
        "output": null,
        "colour": "#333333"
    };
    
    window['Blockly'].Blocks['__example_test'] = {
        init: function() {
            this.jsonInit(__example_test_json);
        }
    };
    
    window['Blockly'].Python['__example_test'] = function(block) {
            return [`HelloWorld`, Blockly.Python.ORDER_CONDITIONAL]
    };
    
    user@ubuntu:~$ 

## Convert java script back to m5b format

    user@ubuntu:~$ jscode2m5b example.jscode > example2.m5b
    user@ubuntu:~$ md5sum example.m5b example2.m5b
    00afc39722114e1bd8c2f968352af40d  example.m5b
    00afc39722114e1bd8c2f968352af40d  example2.m5b
    user@ubuntu:~$ 
